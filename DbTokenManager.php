<?php

namespace yiicod\tokenmanager;

use CApplicationComponent;
use CException;
use CJSON;

/**
 * Token manger for check cross domain request
 *
 * <pre>
 * (id VARCHAR(80) PRIMARY KEY, salt CHAR(80), params VARCHAR(255), dateCreate DATETIME)
 * </pre>
 * Generate:
 * Yii::app()->tokenManager->generate(array('id' => $model->id))
 *
 * Load:
 * $token = Yii::app()->tokenManager->load($token);
 *
 * Validate hash:
 * Yii::app()->tokenManager->validate($token);
 *
 */
abstract class DbTokenManager extends CApplicationComponent
{

    /**
     * In second (1day)
     * @var int
     */
    public $liveTime = 86400;

    /**
     * Use user ip or nope
     * @var bool
     */
    public $currentIp = false;

//    /**
//     * If set true, then token will be regenerated each time after expiry.
//     */
//    public $refresh = true;


    /**
     * Auto create table
     * @var bool
     */
    public $autoCreateTable = true;

    /**
     * Private key
     * @var string
     */
    public $privateKey = '';

    /**
     *
     * @var type
     */
    protected $records;

    /**
     *
     * @var type
     */
    private $salt = null;

    protected function getUserIP()
    {
        if (!$this->currentIp) {
            return '127.0.0.1';
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    /**
     * Get hash salt
     * @return string
     */
    protected function getSalt()
    {
        return $this->salt;
    }

    protected function getToken($params)
    {
//        if (false === $this->refresh) {
//            unset($params['expiryDate']);
//        }
        if (empty($this->privateKey)) {
            throw new CException('Private key is required.');
        }
        $this->salt = utf8_encode(md5(CJSON::encode($params))) . $this->privateKey;

        $token = urlencode(
            hash("sha256", md5($this->getUserIP()) . $this->salt)
        );

        return $token;
    }

    /**
     * Generate and write to db token with params.
     * @params Array $params Additional data for token.
     * @return string Return token.
     */
    abstract public function generate($params);

    /**
     * Load token row from db
     *   if (isset($this->records[$token])) {
     *       return $this->records[$token];
     *   }
     *   //Do load
     *   return $this->records[$token];
     *
     * @return mixed Return array if finded row or false if not finded.
     */
    abstract public function load($token);

    /**
     * Validate token.
     * @return boolean
     */
    abstract public function validate($value);

    /**
     * Create table
     */
    abstract public function createTable();

    /**
     * Delete record by token
     */
    abstract public function delete($token);

    /**
     * Clear old token
     */
    abstract public function clear();

    /**
     * Init manager
     */
    public function init()
    {
        parent::init();
        $this->clear();
    }

}
