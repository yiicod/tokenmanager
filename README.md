Token MongoDB/MySQL generator 
=============================

With this extension you easy generate token on one application and
use this token on other application. Tokens are saved on the MongDB/MySQL.

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist yiicod/tokenmanager "*"
```

or add

```json
"yiicod/tokenmanager": "*"
```

Config ( This is all config for extensions ):
---------------------------------------------

```php
'components' => [
    ...
    'tokenManager' => [
        'class' => 'yiicod\tokenmanager\MongoDbPhp7TokenManager',
        'connectionString' => 'mongodb://IP:PORT',
        'privateKey' => 'Required',
        'dbName' => 'DB name',
        'collectionName' => 'Collection name',
    ]
]

```
Generate token:
---------------
```php
Yii::app()->tokenManager->generate(array('id' => $model->id))
``` 
Load token:
-----------
```php 
$token = Yii::app()->tokenManager->load($token);
```
Validate token:
---------------
```php 
Yii::app()->tokenManager->validate($token);
```