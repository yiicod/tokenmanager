<?php

namespace yiicod\tokenmanager;

use CDbException;
use CException;
use CJSON;
use Yii;

/**
 * Use example
 * 'tokenManager'=>array(
 *     'class' => 'yiicod\tokenmanager\MongoDbTokenManager',
 *     'tableName' => 'YiiDbToken',
 *  ),
 * ),
 */
class MySqlTokenManager extends DbTokenManager
{

    /**
     * Table name
     * @var string
     */
    public $tableName = 'YiiDbToken';

    /**
     * Generate token
     * @param array $params
     * @return string $token
     * @throws CException
     */
    public function generate($params = [])
    {
        $hash = $this->getToken($params);
        if (isset($this->records[$hash])) {
            $token = $this->records[$hash]['token'];
        } else {
            $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName)
                ->where('hash=:hash', [':hash' => $hash])
                ->queryColumn();

            if (true === empty($data)) {
                $time = time();
                $params = array_merge([
                    'expiryDate' => date('Y-m-d H:i:s', $time + $this->liveTime)
                ], $params);
                $token = $this->getToken($params);

                $salt = $this->getSalt();
                $opts = $this->options;
                $opts['upsert'] = true;
                $attrs = [
                    'token' => $token,
                    'hash' => $hash,
                    'salt' => $salt,
                    'params' => CJSON::encode($params),
                    'expiryDate' => $params['expiryDate']
                ];

                try {
                    $update = Yii::app()->db->createCommand()
                        ->insert($this->tableName, $attrs);
                } catch (CDbException $e) {
                    unset($attrs['token']);
                    $update = Yii::app()->db->createCommand()
                        ->update($this->tableName, $attrs, 'token="' . $token . '"');
                }
                if (!$update) {
                    throw new CException('Can not create token', 500);
                }

                $this->records[$hash] = $attrs;
            } else {
                $token = $data['token'];
            }
        }
        return $token;
    }

    /**
     * Load token
     * @param string $token
     * @return boolean
     */
    public function load($token)
    {
        if (isset($this->records[$token])) {
            return $this->records[$token];
        }
        $model = Yii::app()->db->createCommand()
            ->select('*')
            ->from($this->tableName)
            ->where('token=:token', ['token' => $token])
            ->queryRow();
        if (false === $model) {
            return false;
        }
        $model['params'] = CJSON::decode($model['params']);
        $this->records[$token] = $model;
        return $this->records[$token];
    }

    /**
     * Validate token. Is token equal generated token.
     * @param string $token
     * @return bool
     * @throws CException
     */
    public function validate($token)
    {
        $model = $this->load($token);

        $generateToken = null;
        if ($model) {
            $generateToken = $this->getToken($model['params']);
        }

        return $token == $generateToken;
    }

    /**
     * Creates the session DB table.
     */
    public function createTable()
    {
        if ($this->autoCreateTable) {
            Yii::app()->db->createCommand()->createTable($this->tableName, [
                'token' => 'VARCHAR(80) PRIMARY KEY',
                'hash' => 'VARCHAR(80)',
                'salt' => 'VARCHAR(80)',
                'params' => 'VARCHAR(255)',
                'expiryDate' => 'DATETIME',
            ]);
        }
    }

    /**
     * Delete record
     * @param string $token
     */
    public function delete($token)
    {
        try {
            Yii::app()->db->createCommand()->delete($this->tableName, 'token=' . $token);
        } catch (CDbException $e) {
            $this->createTable();
        }
    }

    /**
     * Remove old record
     */
    public function clear()
    {
        if ($this->liveTime === false) {
            return true;
        }
        try {
            Yii::app()->db->createCommand()->delete($this->tableName, 'expiryDate<"' . date('Y-m-d H:i:s', time()) . '"');
        } catch (CDbException $e) {
            $this->createTable();
        }
    }

}
