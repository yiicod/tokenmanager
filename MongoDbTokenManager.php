<?php

namespace yiicod\tokenmanager;

use CException;
use CJSON;
use Mongo;
use MongoClient;
use Yii;

/**
 * Use example
 * 'tokenManager'=>array(
 *     'class' => 'yiicod\tokenmanager\MongoDbTokenManager',
 *     'connectionString' => 'localhost:27017',
 *     'dbName' => 'dbTokenManager',
 *     'collectionName' => 'YiiDbToken',
 *  ),
 * ),
 */
class MongoDbTokenManager extends DbTokenManager
{

    /**
     * @var string Mongo Db host + port
     */
    public $connectionString = "localhost:27017";

    /**
     * @var string Mongo Db Name
     */
    public $dbName = "dbTokenManager";

    /**
     * @var string Collection name
     */
    public $collectionName = "YiiDbToken";

    /**
     * @var Mongo mongo Db connection
     */
    private $connection;

    /**
     * @var Mongo mongo Db collection
     */
    private $collection;

    /**
     * @var array insert options
     */
    private $options;

    /**
     * @var boolean forces the update to be synced to disk before returning success.
     */
    public $fsync = false;

    /**
     * @var boolean the program will wait for the database response.
     */
    public $safe = false;

    /**
     * @var bool
     */
    public $ensureIndex = true;  //set to false after first use of the cache

    /**
     * Connect to mongoDb with life time
     */
    public function connection()
    {
        if ($this->connection === null) {
            $this->connection = new MongoClient($this->connectionString);
            $dbName = $this->dbName;
            $collectionName = $this->collectionName;
            $this->collection = $this->connection->$dbName->$collectionName;
            $this->options = [
                'fsync' => $this->fsync,
                'w' => $this->safe
            ];
        }
    }

    /**
     * Initializes the route.
     * This method is invoked after the route is created by the route manager.
     */
    public function init()
    {
        $this->connection();

        if ($this->ensureIndex) {
            $this->getCollection()->createIndex(['token' => 1]);  // create index on "token"
            $this->getCollection()->createIndex(['hash' => 1]);  // create index on "token"
        }

        parent::init();
    }

    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param array $params
     * @return string
     * @throws CException
     */
    public function generate($params = [])
    {
        $hash = $this->getToken($params);
        if (isset($this->records[$hash])) {
            $token = $this->records[$hash]['token'];
        } else {
            $data = $this->getCollection()->find(['hash' => $hash]);

            if (null === $data) {
                $time = time();
                $params = array_merge([
                    'expiryDate' => date('Y-m-d H:i:s', $time + $this->liveTime)
                ], $params);
                $token = $this->getToken($params);

                $salt = $this->getSalt();
                $opts = $this->options;
                $opts['upsert'] = true;
                $attrs = [
                    'token' => $token,
                    'hash' => $hash,
                    'salt' => $salt,
                    'params' => CJSON::encode($params),
                    'expiryDate' => strtotime($params['expiryDate'])
                ];

                $update = $this->getCollection()->update($attrs, $opts);
                if (!$update) {
                    throw new CException('Can not create token', 500);
                }

                $this->records[$hash] = $attrs;
            }else{
                $token = $data['token'];
            }
        }
        return $token;
    }

    /**
     * Load token
     * @param string $token
     * @return boolean
     */
    public function load($token)
    {
        if (isset($this->records[$token])) {
            return $this->records[$token];
        }

        $model = $this->getCollection()->findOne(['token' => $token], ['token',
            'salt',
            'params',
            'expiryDate'
        ]);

        if (null === $model) {
            return false;
        }
        $model['params'] = CJSON::decode($model['params']);
        $this->records[$token] = $model;
        return $this->records[$token];
    }

    /**
     * Validate token. Is token equal generated token.
     * @param $token
     * @return bool
     * @throws CException
     */
    public function validate($token)
    {
        $model = $this->load($token);

        $generateToken = null;
        if ($model) {
            $generateToken = $this->getToken($model['params']);
        }

        return $token == $generateToken;
    }

    /**
     * Creates the token DB table.
     * @return bool
     */
    public function createTable()
    {
        return true;
    }

    /**
     * Delete record
     * @param string $token
     */
    public function delete($token)
    {
        $this->options['justOne'] = true;
        $this->getCollection()->remove(
            ['token' => ['$eq' => $token]], $this->options);
        unset($this->options['justOne']);
    }

    /**
     * Remove old record
     */
    public function clear()
    {
        if ($this->liveTime === false) {
            return true;
        }
        $this->options['justOne'] = false;
        $this->getCollection()->remove(
            ['expiryDate' => ['$lt' => time()]], $this->options);
        unset($this->options['justOne']);
    }

}
